function updateClock ( )
{
   
  var currentTime = new Date ();

  var currentHours = currentTime.getHours ( );
  var currentMinutes = currentTime.getMinutes ( );
  var currentSeconds = currentTime.getSeconds ( );

  // Pad the minutes and seconds with leading zeros, if required
  currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
  currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

  // Choose either "AM" or "PM" as appropriate
  var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";

  // Convert the hours component to 12-hour format if needed
  currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

  // Convert an hours component of "0" to "12"
  currentHours = ( currentHours == 0 ) ? 12 : currentHours;

  // Compose the string for display
  var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;

  // Update the time display
  document.getElementById("clock").firstChild.nodeValue = currentTimeString;
  document.getElementById("london").firstChild.nodeValue=  calcTime('London ', '+1');
  document.getElementById("egypt").firstChild.nodeValue=  calcTime('Egypt ', '+2');
  document.getElementById("jordan").firstChild.nodeValue=  calcTime('Jordan ', '+3');
  document.getElementById("lebanon").firstChild.nodeValue=  calcTime('Lebanon ', '+3');
  document.getElementById("turkey").firstChild.nodeValue=  calcTime('Turkey ', '+3');
  document.getElementById("colombia").firstChild.nodeValue=  calcTime('Colombia ', '+5');
  document.getElementById("sa").firstChild.nodeValue=  calcTime('South Africa ST ', '+2');
  document.getElementById("angola").firstChild.nodeValue=  calcTime('Angola ', '+5');
  
}
$('document').ready(function(){
   
    updateClock();
    setInterval('updateClock()', 1000 );

});
function calcTime(city, offset) {

    // create Date object for current location
    d = new Date();
  

    // convert to msec
    // add local time zone offset
    // get UTC time in msec
    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
   
    // create new Date object for different city
    // using supplied offset
    nd = new Date(utc + (3600000*offset));
   
    // return time as a string
    return ""+ city + nd.toLocaleString('en-IL');

}
function check(){
    var request = new XMLHttpRequest()

    request.open('GET', 'http://worldtimeapi.org/api/timezone/Europe', true)
    request.onload = function () {
      // Begin accessing JSON data here
      var data = JSON.parse(this.response)
    
      if (request.status >= 200 && request.status < 400) {
        data.forEach((movie) => {
          console.log(movie)
          

        })
      } else {
        console.log('error')
      }
    }
    
    request.send();
}






